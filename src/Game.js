import React from 'react';
import Board from './Board';
import {inject, observer} from 'mobx-react';

class Game extends React.Component {
    render() {
        const {gameStore} = this.props;
        const {handleClick, current, status, draw, resetGame} = gameStore;
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i) => handleClick(i)}
                        status={status}
                        draw={draw}
                        resetGame={resetGame}
                    />
                </div>
            </div>
        );
    }
}

export default inject('gameStore')(observer(Game));

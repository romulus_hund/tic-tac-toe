import GameStore from './stores/GameStore';

const bootstrapper = () => {
    const gameStore = new GameStore();

    return {gameStore};
};

export default bootstrapper;
import {action, observable, runInAction, computed, decorate } from 'mobx';

class GameStore {
    isNextMoveX = true;
    step = 0;
    movesHistory = [{ squares: Array(9).fill(null) }];

    get current(){
        return this.movesHistory[this.step];
    }

    handleClick = (i) => {
        const history = this.movesHistory.slice(0, this.step + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        const winner = this._getWinner(squares);

        if (winner || squares[i]) {
            return;
        }

        squares[i] = this.isNextMoveX ? 'x' : 'o';

        runInAction(()=> {
            this.movesHistory = history.concat({squares: squares});
            this.isNextMoveX = !this.isNextMoveX;
            this.step =  history.length;

        });
    }

    get status () {
        const current = this.movesHistory[this.step];
        const winner = this._getWinner(current.squares);

        if (winner) {
            return  'Winner: ' + winner;
        } else {
            return  'Next player: ' + (this.isNextMoveX ? 'x' : '0');
        }
    }

    _getWinner = squares => {
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[b] === squares[c]) {
                return squares[a];
            }
        }

        return null;
    }

    get draw(){
        const history = this.movesHistory.slice(0, this.step + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        const winner = this._getWinner(squares);
        const isContainNull = this.current.squares.includes(null);
        if(!winner && !isContainNull){
            return 'Draw!';
        }
        return null;
    }

    resetGame = () => {
        this.step = 0;
        this.isNextMoveX = true;
    }

}

export default decorate(GameStore, {
    isNextMoveX: observable,
    step: observable,
    movesHistory: observable,
    current: computed,
    handleClick: action,
    status: computed,
    draw: computed,
    resetGame: action
});

import React from 'react';

export default class Square extends React.Component {
    render() {
        const {onClick, square} = this.props;
        return (
            <button className="square" onClick={onClick}>
                {square}
            </button>
        );
    }
}

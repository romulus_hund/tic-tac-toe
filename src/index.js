import React from 'react';
import ReactDOM from 'react-dom';
import './style.css';
import Game from './Game';
import * as serviceWorker from './serviceWorker';
import bootstrapper from './bootstrapper';
import {Provider} from 'mobx-react';

const injectables = bootstrapper();

ReactDOM.render(<Provider  {...injectables}><Game /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
